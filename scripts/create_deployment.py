"""Create a Deployment record"""
import logging
import os
import pytz
import semver

from pathlib import Path
from salesforce_toolkit import Salesforce
from salesforce_toolkit.exceptions import SalesforceResourceNotFound, SalesforceMalformedRequest
from datetime import datetime

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
cf = logging.Formatter('%(levelname)s:%(name)s - %(message)s') # Define formatter
ch.setFormatter(cf)
LOGGER.addHandler(ch)

def main(SF_ADMIN_USER: str = None,
        SF_ADMIN_PW: str = None,
        SF_ADMIN_TOKEN: str = None,
        SF_DOMAIN: str = None,
        ACAD_ID: str = None,
        CI_PROJECT_ID: str = None,
        CI_PROJECT_PATH: str = None,
        CI_PIPELINE_ID: str = None,
        CI_JOB_ID: str = None,
        CI_COMMIT_REF_NAME: str = None,
        CI_COMMIT_TAG_MESSAGE: str = None,
        PACKAGE_URL: str = None,
        RUN_REPO_TESTS: bool = True,
        **_):
    LOGGER.info(f'Creating deployment record for App Cloud App Details {ACAD_ID}')
    if SF_ADMIN_USER and SF_ADMIN_PW and SF_ADMIN_TOKEN:
        sf = Salesforce(
                username=SF_ADMIN_USER,
                password=SF_ADMIN_PW,
                security_token=SF_ADMIN_TOKEN,
                domain=SF_DOMAIN
            )
        if ACAD_ID:
            is_acad_valid(sf, ACAD_ID, CI_PROJECT_PATH)

            target = 'prod' if is_release_update(CI_COMMIT_REF_NAME) else 'uat'
            # Inserting these values into Release Note Summary for now
            # We could use the GitLab project path, but project Id seems more straightforward
            # Proof of concept showing we can add this to deployment record
            release_notes = (
                f'GitLab Project Id: {CI_PROJECT_ID}\n'
                f'Commit Branch or Tag: {CI_COMMIT_REF_NAME}\n'
                f'Pipeline Id: {CI_PIPELINE_ID}\n'
                f'Job Id: {CI_JOB_ID}\n'
                f'Package URL: {PACKAGE_URL}\n\n'
                f'Tag Message:\n'
                f'{CI_COMMIT_TAG_MESSAGE}'
            )
            LOGGER.info(f'Salesforce target instance is {target}')

            try:
                result = sf.Deployment__c.create({
                    'App_Cloud_App_Detail__c': ACAD_ID,
                    'Target_Instance__c': target,
                    'Run_Repo_Tests__c': RUN_REPO_TESTS,
                    'Release_Note_Summary__c': release_notes,
                    'Start_Time__c': datetime.now(pytz.UTC).isoformat()
                })

                if (result['success'] == True):
                    id = result['id']
                    LOGGER.info(f'Successfully created deployment record: https://heb.lightning.force.com/lightning/r/Deployment__c/{id}/view')
                else:
                    errors = result['errors']
                    raise Exception('Unsuccessful creating deployment record', errors)
            except Exception as ex:
                LOGGER.exception('Exception creating deployment record', ex)
                return False, str(ex)
        else:
            raise Exception('ACAD ID not provided')
    else:
        raise Exception('Authentication credentials were not provided')
    
def is_release_update(branch_or_tag_name: str) -> bool:
    try:
        semver.Version.parse(branch_or_tag_name)
        return True
    except ValueError:
        return False
    
def is_acad_valid(sf: Salesforce, acad_id: str, project_path: str):
    try:
        acad = sf.App_Cloud_App_Details__c.get(acad_id)
        git_repo_path = acad['Git_Repo_Path__c']

        if git_repo_path != project_path:
            err_msg = (
                'Git Repo Path on App Cloud App Details does not match GitLab CI project path\n'
                f'App Cloud App Details Git Repo Path: {git_repo_path}\n'
                f'CI project path: {project_path}\n'
            )
            raise Exception(err_msg)
    except (SalesforceResourceNotFound, SalesforceMalformedRequest):
        raise Exception('App Cloud App Detail Id is not valid')
    except Exception as ex:
        LOGGER.exception('Exception while validating App Cloud App Details', ex)

if __name__ == '__main__':
    local_config_path = Path(__file__).parent.parent / 'local.config.yaml'
    if local_config_path.exists():
        import yaml
        with local_config_path.open() as local_config_file:
            local_config = yaml.safe_load(local_config_file)
            main(**local_config)
    elif os.environ.get('CI'):
        # running in a Gitlab CI job
        LOGGER.setLevel(level=logging.INFO)
        main(**os.environ)
